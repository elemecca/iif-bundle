<?php

namespace BetaMFD\IifBundle\Journal;

class JournalTransaction extends \BetaMFD\IifBundle\Transaction
{
    protected $columns = [
        '!TRNS' => [
            'trnsId',
            'trnsType',
            'date',
            'accnt',
            'class',
            'amount',
            'docNum',
            'memo',
        ],
        '!SPL' => [
            'splId',
            'trnsType',
            'date',
            'accnt',
            'class',
            'amount',
            'docNum',
            'memo',
        ],
        '!ENDTRNS' => [],
    ];

    public function addRow($account, $amount, $memo = '') {
        if (empty($this->rows)) {
            $bang = 'TRNS';
        } else {
            $bang = 'SPL';
        }
        $this->addAnyRow(new \BetaMFD\IifBundle\TransactionRow([
            'trnsType' => 'GENERAL JOURNAL',
            'bang' => $bang,
            'docNum' => $this->docNum,
            'accnt' => $account,
            'date' => $this->date,
            'amount' => $amount,
            'memo' => $memo,
        ]));
    }
}
