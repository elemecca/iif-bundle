<?php

namespace BetaMFD\IifBundle\Journal;

class Journal extends \BetaMFD\IifBundle\Group
{
    protected $columns = [
        '!TRNS' => [
            'trnsId',
            'trnsType',
            'date',
            'accnt',
            'class',
            'amount',
            'docNum',
            'memo',
        ],
        '!SPL' => [
            'splId',
            'trnsType',
            'date',
            'accnt',
            'class',
            'amount',
            'docNum',
            'memo',
        ],
        '!ENDTRNS' => [],
    ];

    //I think this can go either way
    protected $headerAmountDirection = '';

    //I think this can go either way
    protected $rowAmountDirection = '';

    public function addRow($docNum, $account, $date, $amount, $memo = '') {
        if (empty($this->transactions[$docNum])) {
            $this->transactions[$docNum] = new JournalTransaction($docNum, $date, $this->columns);
        }
        $this->transactions[$docNum]->addRow($account, $amount, $memo);
    }

    public function makeTransaction($docNum, $date)
    {
        throw new \Exception("addRow() creates Journal transactions. Don't make transactions like this for Journal Entries.");
    }

}
