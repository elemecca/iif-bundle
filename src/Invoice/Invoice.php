<?php

namespace BetaMFD\IifBundle\Invoice;

class Invoice extends \BetaMFD\IifBundle\Group
{
    protected $transType = 'INVOICE';

    protected $headerAmountDirection = '-';

    protected $rowAmountDirection = '-';

    protected $columns = [
        '!TRNS' => [
            'trnsId',
            'trnsType',
            'date',
            'accnt',
            'name',
            'class',
            'amount',
            'docNum',
            'memo',
            'clear',
            'toPrint',
            'dueDate',
            'terms',
            'paid',
            'payMeth',
            'shipVia',
            'shipDate',
            'other1',
            'poNum',
            'toSend',
            'isAJE',
            'nameIsTaxable',
        ],
        '!SPL' => [
            'splId',
            'trnsType',
            'date',
            'accnt',
            'name',
            'class',
            'amount',
            'docNum',
            'memo',
            'clear',
            'qnty',
            'price',
            'invItem',
            'payMeth',
            'taxable',
            'valAdj',
            'reimbExp',
            'extra',
        ],
        '!ENDTRNS' => [],
    ];

    public function addInvoiceInformation($customer, $date, $poNum) {
        $this->customer = $customer;
        $this->date = $date;
        $this->poNum = $poNum;
    }

    public function addItemRow($docNum, $item, $qty, $price, $lineTotal, $memo) {
        $this->makeTransaction($docNum, $this->date);
        $row = new \BetaMFD\IifBundle\TransactionRow([
            'bang' => 'SPL',
            'trnsType' => $this->transType,
            'docNum' => $docNum,
            'date' => $this->date,
            'invItem' => $item,
            'qnty' => -$qty,
            'price' => $price,
            'amount' => $lineTotal,
            'memo' => $memo,
            'taxable' => 'N',
            'valAdj' => 'N',
        ]);
        $this->transactions[$docNum]->addTransSplitRow($row);
    }

    public function addInvoiceHeader($docNum)
    {
        if (empty($this->accountNumber)) {
            throw new \Exception("You need to set an account number before adding the header");
        }
        $this->makeTransaction($docNum, $this->date);
        $due = clone $this->date;
        $due->modify('+30 days');
        $invoiceTotal = 0;
        foreach ($this->transactions[$docNum]->getRows() as $row) {
            if (!empty($row)) {
                $invoiceTotal = bcSub($invoiceTotal, $row->getAmount(), 2);
            }
        }
        $row = new \BetaMFD\IifBundle\TransactionRow([
            'bang' => 'TRNS',
            'trnsType' => 'INVOICE',
            'name' => $this->customer,
            'nameIsTaxable' => 'N',
            'docNum' => $docNum,
            'poNum' => $this->poNum,
            'date' => $this->date,
            'shipDate' => $this->date,
            'dueDate' => $due,
            'terms' => 'NET30',
            'shipVia' => 'Delivery',
            'toPrint' => 'Y',
            'accnt' => $this->accountNumber,
            'paid' => 'N',
            'toSend' => 'N',
            'isAJE' => 'N',
            'amount' => -$invoiceTotal,
        ]);
        $this->transactions[$docNum]->addTransactionRow($row);
    }
}
