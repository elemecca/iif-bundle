<?php

namespace BetaMFD\IifBundle;

class Transaction
{
    protected $columns = [];

    protected $transactionRow;

    /**
     * @var array
     */
    protected $splitRows = [];

    protected $rows;

    /**
     * @var string
     */
    protected $docNum = '';

    /**
     * @var \DateTime
     */
    protected $date;

    public function __construct($docNum, \DateTime $date, $columns)
    {
        $this->docNum = $docNum;
        $this->date = $date;
        $this->columns = $columns;
    }

    public function addAnyRow(TransactionRow $row)
    {
        if ($row->isTransaction() && empty($this->transactionRow)) {
            $this->addTransactionRow($row);
        } else {
            $this->addTransSplitRow($row);
        }
        $this->makeRows();
    }

    public function addTransactionRow(TransactionRow $row)
    {
        $this->transactionRow = $row;
        $this->makeRows();
    }

    public function addTransSplitRow(TransactionRow $row)
    {
        $this->splitRows[] = $row;
        $this->makeRows();
    }

    protected function makeRows()
    {
        $this->rows = array_merge([$this->transactionRow], $this->splitRows);
    }

    /**
     * Colcount must be given from the IIf file,
     * because another transaction may have more columns
     * @param  integer $colCount
     * @return string
     */
    public function getTransactionText($colCount)
    {
        $text = '';
        foreach ($this->rows as $row) {
            $text .= $row->getText($this->columns, $colCount);
        }
        $text .= 'ENDTRNS';
        for ($i = 0; $i < $colCount; $i++) {
            $text .= "\t";
        }
        $text .= "\n";
        return $text;
    }

    public function getColCount(): int
    {
       $colCount = 0;
       foreach ($this->columns as $c) {
           $count = count($c);
           //keep the larger number
           $colCount = $colCount < $count ? $count : $colCount;
       }
       return $colCount;
    }

    /**
     * Get the value of Rows
     *
     * @return array
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Get the value of Doc Num
     *
     * @return string
     */
    public function getDocNum()
    {
        return $this->docNum;
    }

    /**
     * Get the value of Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
