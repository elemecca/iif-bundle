<?php

namespace BetaMFD\IifBundle;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Group
{
    /**
     * @var array
     */
    protected $transactions = [];

    /**
     * @var string
     */
    protected $transType = '';

    //many transactions require a negative header
    protected $headerAmountDirection = '-';

    //many transactions require a postive total for the rows
    protected $rowAmountDirection = '+';

    /**
     * Default account number for transactions.
     * Not all transactions need an account number.
     *
     * @var int
     */
    protected $accountNumber;

    public function getFileContent($colCount) {
        $this->testEntries();
        if (empty($this->transactions)) {
            return '';
        }
        $text = $this->getHeader($colCount);
        foreach ($this->transactions as $t) {
            $text .= $t->getTransactionText($colCount);
        }
        return $text;
    }

    /**
     *
     * @return string
     */
    public function getHeader($colCount)
    {
        //loop to form the header
        $header = '';
        foreach ($this->columns as $key => $columns) {
            $header .= "$key\t";
            $header .= implode("\t", $columns);
            $thisCount = count($columns);
            if ($thisCount < $colCount) {
                $difference = $colCount - $thisCount;
                if ($thisCount == 0) {
                    //I don't know why
                    $difference--;
                }
                for ($i = 0; $i < $difference; $i++) {
                    $header .= "\t";
                }
            }
            $header .= "\n";
        }
        //upper case everything so I can use lower case on the columns
        return strtoupper($header);
    }

    /**
     * Gets a clumn count for each transaction and returns the highest count
     * @return integer $colCount
     */
    public function getColCount(): int
    {
        $colCount = 0;
        foreach ($this->transactions as $t) {
            $count = $t->getColCount();
            //keep the larger number
            $colCount = $colCount < $count ? $count : $colCount;
        }
        return $colCount;
    }

    public function testEntries()
    {
        foreach ($this->transactions as $t) {
            $amount = 0;
            $header_amount = 0;
            $row_amount = 0;
            foreach ($t->getRows() as $row) {
                $amount = bcadd($amount, $row->getAmount(), 2);
                if ($row->isTransaction()) {
                    $header_amount = bcadd($header_amount, $row->getAmount(), 2);
                } else {
                    $row_amount = bcadd($row_amount, $row->getAmount(), 2);
                }
            }
            if ($this->headerAmountDirection === '+' and $header_amount < 0) {
                throw new \Exception('Header should be positive and it\'s negative.');
            }
            if ($this->headerAmountDirection === '-' and $header_amount > 0) {
                throw new \Exception('Header should be negative and it\'s positive.');
            }
            if ($this->rowAmountDirection === '+' and $row_amount < 0) {
                throw new \Exception('Row totals should be positive and came out negative.');
            }
            if ($this->rowAmountDirection === '-' and $row_amount > 0) {
                throw new \Exception('Row totals should be negative and came out positive.');
            }
            if (($this->rowAmountDirection === '-' and $this->headerAmountDirection === '-')
                or ($this->rowAmountDirection === '+' and $this->headerAmountDirection === '+')
            ) {
                //both header and rows are the same
                $dif = bcsub($header_amount, $row_amount, 2);
                if ($dif != 0) {
                    throw new \Exception('Transaction is out of balance by $' . number_format($dif, 2));
                }
            } elseif ($amount != 0) {
                throw new \Exception('Transaction is out of balance by $' . number_format($amount, 2));
            }
        }
    }

    public function makeTransaction($docNum, $date)
    {
        if (empty($this->transactions[$docNum])) {
            $this->transactions[$docNum] = new Transaction($docNum, $date, $this->columns);
        }
    }

    /**
     * Get the Transactions
     *
     * @return array
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Get the value of Account Number
     *
     * @return int
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set the value of Account Number
     *
     * @param int $accountNumber
     *
     * @return self
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }
}
