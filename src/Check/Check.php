<?php

namespace BetaMFD\IifBundle\Check;

class Check extends \BetaMFD\IifBundle\Group
{
    protected $transType = 'CHECK';

    protected $columns = [
        '!TRNS' => [
            'trnsId',
            'trnsType',
            'date',
            'accnt',
            'name',
            'class',
            'amount', //negative
            'docNum',
            'clear', // Y/N
            'toPrint', // Y/N
        ],
        '!SPL' => [
            'splId',
            'trnsType',
            'date',
            'accnt',
            'name',
            'class',
            'amount', //positive
            'docNum',
            'clear', // Y/N
        ],
        '!ENDTRNS' => [],
    ];

    public function addRow($docNum, $date, $account, $lineTotal) {
        $this->makeTransaction($docNum, $date);
        $row = new \BetaMFD\IifBundle\TransactionRow([
            'bang' => 'SPL',
            'trnsType' => $this->transType,
            'date' => $date,
            'accnt' => $account,
            'name' => '',
            'class' => '',
            'amount' => $lineTotal,
            'docNum' => $docNum,
            'clear' => 'N',
        ]);
        $this->transactions[$docNum]->addTransSplitRow($row);
    }

    public function addHeader($docNum, $date, $payTo, $checkAmount, $toPrint = 'N')
    {
        if (empty($this->accountNumber)) {
            throw new \Exception("You need to set an account number before adding the header");
        }
        $this->makeTransaction($docNum, $date);
        $row = new \BetaMFD\IifBundle\TransactionRow([
            'bang' => 'TRNS',
            'trnsType' => $this->transType,
            'date' => $date,
            'accnt' => $this->accountNumber,
            'name' => $payTo,
            'class' => '',
            'amount' => -$checkAmount,
            'docNum' => $docNum,
            'clear' => 'N',
            'toPrint' => $toPrint,
        ]);
        $this->transactions[$docNum]->addTransactionRow($row);
    }
}
