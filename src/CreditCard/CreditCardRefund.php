<?php

namespace BetaMFD\IifBundle\CreditCard;

class CreditCardRefund extends \BetaMFD\IifBundle\Check\Check
{
    protected $transType = 'CREDIT CARD REFUND';

    protected $headerAmountDirection = '+';

    protected $rowAmountDirection = '-';
}
