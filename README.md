I needed something to generate IIF files across multiple Symfony projects.
It doesn't use entities or services so will probably work just fine with non-symfony projects as well.
I mostly called it "bundle" out of habit and because I don't know how it may develop.

### Composer Install ###
Add a vcs entry to your composer.json file_entity
```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/betaMFD/iif-bundle.git"
        }
    ],
    "require": {
        "betamfd/iif-bundle": "dev-master"
    },


```

Then require it
```
composer require betamfd/iif-bundle dev-master
```


### Enable the Bundle
At this time this "bundle" doesn't need to be enabled. Not sure it
counts as a bundle at this point.


### Using these files
TBA


### Limitations ###
In addition to the things I simply haven't bothered coding yet:
* QuickBooks cannot import Bill Payments. You can, however, import a check/credit card to Accounts Payable and use the bill pay section to apply the credit.
